﻿namespace BanksySan.Demo.DynamicLoadTypes.TestAssembly1
{
    public class Foo : IFoo {
        public string Echo(string message)
        {
            return $"Echo {message}";
        }
    }
}