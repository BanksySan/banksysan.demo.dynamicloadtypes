﻿namespace BanksySan.Demo.DynamicLoadTypes
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using NUnit.Framework;

    [TestFixture]
    internal class LoadingAssemblyTests
    {
        private const string TEST_ASSEMBLY = @"BanksySan.Demo.DynamicLoadTypes.TestAssembly1";

        [Test]
        public void CanLoadAssembly()
        {
            Assembly.Load(TEST_ASSEMBLY);
        }

        [Test]
        public void CanCreateConcreteType()
        {
            var assembly = Assembly.Load(TEST_ASSEMBLY);

            var types = assembly.GetTypes();
            Debug.Print(string.Join("; ", types.Select(x => x.Name)));
            var fooType = types.Single(x => x.Name == "Foo");

            var foo = Activator.CreateInstance(fooType);

            Assert.That(foo, Is.AssignableTo<IFoo>());

            var iFoo = (IFoo) foo;

            var message = "Hello";
            var echo = iFoo.Echo(message);
            Assert.That(echo, Is.EqualTo($"Echo {message}"));
        }
    }
}