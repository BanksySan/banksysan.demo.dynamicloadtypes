﻿namespace BanksySan.Demo.DynamicLoadTypes
{
    public interface IFoo
    {
        string Echo(string message);
    }
}